/*
	App: Booking System API
	Scenario:
		A course booking sytem apllication where a user can enroll into a course

	Type: COurse Booking System (Web App)

	Description:
		A course booking system aplication where a iser can enroll into a course

		it allows an admin to do CRUD operations
		it allows users to regiter into our database

	Features:

		-User registration //add user
		-user autentication //user login
		

		Customer/Authenticated Users:
		-View COurses (All Active COurses)
		-Enroll COurse


		Admin Users:
		-create a course
		-Update the course
		-delert/edit/archive/unarchive (active/inactive) //soft deletion/reativate the course
		-view courses  (All courses active/inactive)
		-view/mamager user accounts**S
		

		All Users (guests, customers, admin)
		-view active courses
*/

//Data Model for the booking System
//two-way embedding

/*
	user{
		id - unique identifier document
		firstname,
		lastName,
		email,
		password,
		mobileNumber,
		isAdmin,
		enrollments:[
			id - document identifier
			courseId0 the unique identifier for course
			courseName - optional
			status,
			dateEnrolled - optional
		]
	}



*/
/*
	course{
		id - unique identitfier for the document
		name,
		description,
		price,
		isActive,
		createdOn,
		enrollees:[
			id - document identifier
			USERid,
			isPaid,
			dateEnrolled - optional

		]

	
	}


*/