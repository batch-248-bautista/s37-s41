//use User Model
const User = require("../models/User");

//use course model
const Course = require("../models/Course")

const bcrypt = require("bcrypt");
const auth = require("../auth");

//check if the email already exists
/*
	Steps:
	1. Use mongoose "find"method to find aduplicate email
	2. use the "then" method to send a response back to the frontend based on the result of the "find" method


*/

module.exports.checkEmailExists = (reqBody) =>{
	// the result is sent back to frontend via the .then methid found in the route file

	return User.find({email: reqBody.email}).then(result =>{
		// the find method reurns a record if a match is found
		if (result.length>0){
			return true;
		}
		//no duplicate email found
		// user is not yet registred in the db
		else{
			return false
		}
	})
};

//user registration
/*
	Steps:
	1. Creates a new USer object using the mongoose model and the information from the reqBody
	2. Make user that the password is encrypted
	3. save the new user passwoed to the datbase

*/

module.exports.registerUser = (reqBody) =>{
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		password: bcrypt.hashSync(reqBody.password, 10)
	})
	//ten rounds of hashing
	//jwt - json web token
	return newUser.save().then((user, error)=>{
		if(error){
			return false
		}
		else{
			return true
		}
	})
}

// User authentication
/*
	Steps:
	1. Check the database if the user email exists
	2. Compare the password provided in the login form with the password stored in the database
	3. Generate/return a JSON web token if the user is successfully logged in and return false if not
*/

module.exports.loginUser = (reqBody)=>{
	return User.findOne({email: reqBody.email}).then(result=>{
		if(result == null){
			return false;
		}
		else{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password,  result.password)
			if(isPasswordCorrect){
				return{access: auth.createAccessToken(result)}
			}
		}
	})
}

//create a getProfilecontroller metho for retrieving the details of the user
module.exports.getProfile = (data)=>{
	
	return User.findById(data.userId)
	 .then(result=>{
		result.password = "";
		return result;
		
		})
		
		
	}

//enroll user to a course
/*
	Steps:
	1. Find the documents in the db using the users's ID.
	2. Ass the course Id to user's enrollement array
	3. Update the document in the MongoDb

*/
//async await wil be used in enrolling the user because we will need youpdate 2 separate documents when enrolling a user
module.exports.enroll = async (data) => {
	let isUserUpdated = await User.findById(data.userId).then(user=>{
		user.enrollments.push({courseId:data.courseId});
		return user.save().then((user, error)=>{
			if(error){
				return false
			}
			else{
				return true;
			}
		})
	})
	let isCourseUpdated = await Course.findById(data.courseId).then(course=>{
		course.enrollees.push({userId: data.userId});
		return course.save().then((course, error)=>{
			if(error){
				return false;
			}
			else{
				return true
			}
		})
	})
	//if the courseUpdated and userUpdated saved a document then definitely it return true kaya ginamit yang condition na yan
	if (isUserUpdated && isCourseUpdated){
		return true
	}
	else{
		return false;
	}

}

//enroll the sample user into two or more courses
//send a screecap of the document with now 2 sub documents for theboth user and one each 2 courses