const Course = require("../models/Course");

/*module.exports.addCourse = (reqBody) =>{
	let newCourse = new Course({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	});
	return newCourse.save().then((course,error)=>{
		if (error){
			return false
		}
		else{
			return true
		}
	})
}*/

module.exports.addCourse = (data) =>{
	if(data.isAdmin){
		let newCourse = new Course({
			name: data.course.name,
			description: data.course.description,
			price: data.course.price
		});
		return newCourse.save().then((course,error)=>{
			if (error){
				return false
			}
			else{
				return true
			}
		})
	}
	else{
		return false
	}
	
	
}

//retrieve all the courses from the databse
/*
Steps:
1.REtrive all the courses form the database

*/

module.exports.getAllCourses =()=>{
	return  Course.find({}).then(result=>{
		return result;
	})
}


//retrive all actuve courses
/*
	Steps:
	1. Retrive all the courses from the database with the roperty "isActive" witht the value true

*/
module.exports.getAllActive =()=>{
	return  Course.find({isActive: true}).then(result=>{
		return result;
	})
}

//update a course
/*
	Steps:
	1.Create a variable "updatedCourse" which will contain information from the reqBody
	2. Find and update the course using the courseId retrieved from the reqParams property and the variable "updateCourse" containing the information from the reqBody

*/
module.exports.updateCourse = (reqParams, reqBody) =>{
	let updatedCourse = {
		name : reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	}
	//findByIdAndUpdate(document ID, updatesToBeApplied)
	return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((course, error)=>{
		if(error){
			return false
		}
		else{
			return true
		}
	})
}




// //archive course
// module.exports.archiveCourse = (reqParams, reqBody) =>{
// 	let archiveCourse ={
// 		isActive: reqBody.isActive
// 	}
// 	return Course.findById(reqParams.courseId, archiveCourse).then((course, error)=>{
// 		if(error){
// 			return false
// 		}
// 		else{
// 			return true
// 		}
// 	})
// }

// //unarchive
// module.exports.unarchiveCourse = (reqParams, reqBody) =>{
// 	let archiveCourse ={
// 		isActive: reqBody.isActive
// 	}
// 	return Course.findById(reqParams.courseId, archiveCourse).then((course, error)=>{
// 		if(error){
// 			return false
// 		}
// 		else{
// 			return true
// 		}
// 	})
// }


//change isActive property vise versa
module.exports.archiveCourse = (reqParams) =>{
	return Course.findById(reqParams.courseId).then((status)=>{
		status.isActive = "false";
		return status;
	})
}

module.exports.unarchiveCourse = (reqParams) =>{
	return Course.findById(reqParams.courseId).then((status)=>{
		status.isActive = "true";
		return status;
	})
}
