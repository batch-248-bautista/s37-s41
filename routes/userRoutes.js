const express = require("express");
const  router = express.Router();

const userController = require("../controllers/userControllers")

const auth = require("../auth");
//route for checking if theusers email already exists in the db
//invoke the checkEmailExists function from the controller file later to controller file later to communicate withour db
//passes the "body" property of our "request" object to the correpsonding controller function
router.post("/checkEmail",(req, res)=>{
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
})


//route for userrregistration
router.post("/register",(req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
})

//route for user authentication
router.post("/login",(req, res)=>{
	userController.loginUser(req.body).then(resultFromController=>res.send(resultFromController));
})


//route that will accpet the user's Id to retrieve the details of the user
router.get("/details",auth.verify,(req, res)=>{

const userData = auth.decode(req.headers.authorization);

	userController.getProfile({userId:userData.id}).then(resultFromController=>res.send(resultFromController));
})

//route ro enroll a user to a course
router.post("/enroll", auth.verify, (req,res)=>{
	let data = {
		courseId: req.body.courseId,
		userId: auth.decode(req.headers.authorization).id
		
	}
	console.log(data);
	userController.enroll(data).then(resultFromController=>res.send(resultFromController));
})

//allows us to ecport the router object thatwill beaccesed in out "index.js"
module.exports = router;