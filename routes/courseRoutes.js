/*
import express
put in a variable express.Router()
import courseControllers
*/

const express = require("express");
const router = express.Router();
const auth = require("../auth");
const courseController = require("../controllers/courseControllers");


//Route for creating a course

router.post("/",auth.verify,(req,res)=>{

	const data = {
		course: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	courseController.addCourse(data).then(resultFromController=>res.send(resultFromController))
})

//route for retrieving all the course
router.get("/all",(req,res)=>{
	courseController.getAllCourses().then(resultFromController=>res.send(resultFromController))
})

//route for retrieving all the active courses
router.get("/",(req,res)=>{
	courseController.getAllActive().then(resultFromController=>res.send(resultFromController));
})




//route for updating a course
//JWT verification is needed for this to ensure that the user is logged in before updating a course
router.put("/:courseId", auth.verify, (req, res)=>{
	courseController.updateCourse(req.params, req.body).then(resultFromController=>res.send(resultFromController))
})

// //archive a course
// //in managing a database, it is a common practice to soft delete our records(archive) and what we would implement in the "delete"operation of our application
// router.patch("/:courseId/archive", auth.verify, (req, res)=>{
// 	courseController.archiveCourse(req.params, req.body).then(resultFromController=>res.send(resultFromController))
// })


// //unarchive
// router.patch("/:courseId/unarchive", auth.verify, (req, res)=>{
// 	courseController.unarchiveCourse(req.params, req.body).then(resultFromController=>res.send(resultFromController))
// })

//change isActive property vise versa
router.patch("/:courseId/archive", auth.verify, (req, res)=>{
	courseController.archiveCourse(req.params, req.body).then(resultFromController=>res.send(resultFromController))
})

router.patch("/:courseId/unarchive", auth.verify, (req, res)=>{
	courseController.unarchiveCourse(req.params).then(resultFromController=>res.send(resultFromController))
})



//allows us to export the "router" object that will be accessed in our "index.js"
module.exports = router;