const mongoose = require("mongoose");

const courseSchema = new mongoose.Schema({
	name: {
		type : String,
		required : [true, "Course is Required!"]
	},
	description : {
		type : String,
		required: [true, "Description is required!"]
	},
	price : {
		type : Number,
		required : [true, "Price is required!"]
	},
	isActive:{
		type : Boolean,
		default: true
	},
	createdOn : {
		type : Date,
		//new date() express instatiates a new "date" that stores to current date adn aslo time whenever a course is created in our database
		default : new Date()
	},
	enrollees:[
		{
			userId :{
				type: String,
				required: [true, "User Id is Required!"]
			},
			enrolledOn:{
				type: Date,
				default: new Date()
			}
		}


	]
})

module.exports = mongoose.model("Course", courseSchema);